#pragma once

class Node{
    int value;
    Node *prev;
    Node *next;

public:
    Node(int value, Node *prev, Node *next) : value(value), prev(prev), next(next) {}

    Node(int value, Node *prev) : value(value), prev(prev), next(nullptr) {}

    Node(int value) : value(value), prev(nullptr), next(nullptr) {}

    ~Node();

    void setPrev(Node *prev);

    void setNext(Node *next);

    Node *getPrev() const;

    Node *getNext() const;

    int getValue() const;
};