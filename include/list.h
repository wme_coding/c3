#pragma once

#include "iterator.h"

class List{
public:
    virtual void add(int value) = 0;

    virtual void remove() = 0;

    virtual void find(int x) = 0;

    virtual void clear() = 0;

    virtual bool isEmpty() = 0;

    virtual Iterator *getFirst() = 0;
};