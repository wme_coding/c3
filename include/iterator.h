#pragma once

class Iterator{
public:
    virtual void start() = 0;

    virtual int get() = 0;

    virtual void next() = 0;

    virtual bool isFinished() = 0;
};