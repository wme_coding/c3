#pragma once

#include "iterator.h"
#include "node.h"
#include "list.h"

class CircularList: public List{
    class ListIterator: public Iterator{
        CircularList *list;
        Node *current;
    public:
        ListIterator(CircularList *list);

        void start() override;

        int get() override;

        void next() override;

        bool isFinished() override;

        friend class CircularList;
    };


    Node *head;
    Node *tail;
    ListIterator iterator;
    int size;

public:
    CircularList() : head(nullptr), tail(nullptr), iterator(this), size(0) {}

    void start();

    int get();

    void next();

    bool isFinished();

    friend class CircularList;

    void printList();

    void add(int value) override;

    void remove() override;

    void find(int x) override ;

    void clear() override ;

    bool isEmpty() override;

    Iterator *getFirst() override ;
};