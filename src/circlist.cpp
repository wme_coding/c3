#include <stdexcept>
#include <iostream>
#include "../include/circlist.h"


void CircularList::add(int value) {
    if(iterator.current == nullptr){
        head = new Node(value);
        tail = head;
        iterator.current = head;
    } else {
        if(iterator.current->getNext() == nullptr){
            Node *newNode = new Node(value, head);
            head->setNext(newNode);
            head = head->getNext();
            iterator.current = head;
        } else {
            Node *temp = iterator.current->getNext();
            Node *newNode = new Node(value, iterator.current, iterator.current->getNext());
            iterator.current->setNext(newNode);
            iterator.current = iterator.current->getNext();
            temp->setPrev(iterator.current);
        }
    }
    ++size;
}

void CircularList::remove() {
    if(size == 1){
        delete head;
        head = nullptr;
        tail = nullptr;
        iterator.current = nullptr;
    } else {
        if(iterator.current->getPrev() == nullptr){
            tail = tail->getNext();
            delete tail->getPrev();
            tail->setPrev(nullptr);
            iterator.current = tail;
        } else if(iterator.current->getNext() == nullptr){
            head = head->getPrev();
            delete head->getNext();
            head->setNext(nullptr);
            iterator.current = head;
        } else{
            iterator.current->getPrev()->setNext(iterator.current->getNext());
            iterator.current->getNext()->setPrev(iterator.current->getPrev());
            Node *temp = iterator.current;
            iterator.current = iterator.current->getNext();
            delete temp;
        }
    }
    --size;
}

void CircularList::find(int x) {
    iterator.start();
    while(!isFinished()){
        if(iterator.current->getValue() == x){
            return;
        }
        iterator.next();
    }

    if(iterator.current->getValue() != x){
        throw std::logic_error("No such number");
    }
}

void CircularList::clear() {
    while(head != tail){
        head = head->getPrev();
        delete head->getNext();
    }
    delete head;
    head = nullptr;
    tail = nullptr;
    iterator.current = nullptr;
    size = 0;
}

bool CircularList::isEmpty() {
    return size == 0;
}

Iterator *CircularList::getFirst() {
    return nullptr;
}

void CircularList::start() {
    iterator.start();
}

int CircularList::get() {
    return iterator.get();
}

void CircularList::next() {
    iterator.next();
}

bool CircularList::isFinished() {
    return iterator.isFinished();
}

void CircularList::printList() {
    start();
    while (!isFinished()){
        std::cout << get() << " | ";
        next();
    }
    std::cout << get() << std::endl;
}

void CircularList::ListIterator::start() {
    if(list->tail == nullptr){
        throw std::logic_error("No elements in list");
    }
    current = list->tail;
}

int CircularList::ListIterator::get() {
    if(current == nullptr){
        throw std::logic_error("Start iterator first");
    }
    return current->getValue();
}

void CircularList::ListIterator::next() {
    if(current == nullptr){
        throw std::logic_error("Start iterator first");
    }
    if(!isFinished()){
        current = current->getNext();
    }
}

bool CircularList::ListIterator::isFinished() {
    if(current == nullptr){
        throw std::logic_error("Start iterator first");
    }
    return current->getNext() == nullptr;
}

CircularList::ListIterator::ListIterator(CircularList *list) : list(list), current(nullptr) {}
