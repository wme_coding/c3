#include <iostream>
#include "list"
#include "../include/circlist.h"

using namespace std;

int main() {
    CircularList list;
    list.add(83);
    list.add(4);
    list.start();
    list.add(2);
    list.next();
    list.add(121);
    list.add(843);
    list.add(431);
    list.add(8);
    list.printList();

    list.find(843);
    cout << list.get() << endl;

    try {
        list.find(25);
    } catch( logic_error error){
        cout << error.what() << endl;
    }

    list.clear();
    cout << list.isEmpty()  << endl;

    try {
        list.start();
    } catch( logic_error error){
        cout << error.what() << endl;
    }

    try {
        list.get();
    } catch( logic_error error){
        cout << error.what() << endl;
    }

}
//369400 369863 369125

