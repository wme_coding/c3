#include "../include/node.h"

void Node::setPrev(Node *prev) {
    Node::prev = prev;
}

void Node::setNext(Node *next) {
    Node::next = next;
}

Node *Node::getPrev() const {
    return prev;
}

Node *Node::getNext() const {
    return next;
}

int Node::getValue() const {
    return value;
}

Node::~Node() {
    next = nullptr;
    prev = nullptr;
}
